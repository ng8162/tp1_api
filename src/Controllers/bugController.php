<?php
namespace BugApp\Controllers;

use BugApp\Models\BugManager;

use GuzzleHttp\Client;


class BugController
{

    public function add(){        
        $bugManager = new BugManager();
        //$content = $this->render('src/Views/add', []);
        $content=$_POST;
        

        if (isset($_POST['description']) && isset($_POST['auteur']) && isset($_POST['domainName'])) {
            if (isset($_POST['etat']) && $_POST['etat']==='on') {
                $etat=1;
            }else{
                $etat=0;
            }
                $domainName = $this->getDomainName($_POST['domainName']);
                
                $responseAPI = $this->RequestApi($domainName);

                if ($responseAPI->status==='success') {
                    $ip = $responseAPI->query;
                    $bugManager->add(htmlspecialchars($_POST['description']), htmlspecialchars($_POST['auteur']),$etat,htmlspecialchars($_POST['domainName']),$ip);
                    //header('Location: ../bug/list');
                    die();
                }else{
                    echo 'Le nom de dommaine saisi est inconnu';
                    die();
                }

                
                
            }
    
         

        return $this->sendHttpResponse($content, 200);
    }

    public function list(){
        /*echo'<pre>';
        var_dump(apache_request_headers());
        echo '</pre>';*/

        $bugManager = new BugManager();
        $headers = apache_request_headers();

            if (true){
                //var_dump($_GET);
                if(isset($_GET['filter'])){
                    if ($_GET['filter'] ==='0') {
                        $bugs = $bugManager->findByStatut(0);
                    }else if ($_GET['filter'] === '1') {
                        $bugs = $bugManager->findByStatut(1);
                    }
                }else{
                    $bugs = $bugManager->findAll();
                }
                /*$response =[
                    'succes' => true,
                    'bugs' => $bugs,
                ];*/
                $response = $bugs;
                echo json_encode($response);
            }else{
                $bugs = $bugManager->findAll();
                $content = $bugs;
                //$content = $this->render('src/Views/list', ['bugs' => $bugs]);
                return $this->sendHttpResponse($content, 200);
            }     
        
        }


    public function show($id){

        $bugManager = new BugManager();        
        $bug = $bugManager->find($id);
        $content = $bug;
        //$content = $this->render('src/Views/show', ['bug' => $bug]);
        
        return $this->sendHttpResponse($content, 200);
    }

    public function edit($id){
        $bugManager = new BugManager();        
        $bug = $bugManager->find($id);
        $content = $this->render('src/Views/edit', ['bug' => $bug]);
        
        return $this->sendHttpResponse($content, 200);
    }

    public function update($id){
        // Récupérer les données en PATCH

        echo parse_str(file_get_contents('php://input'), $_PATCH);

        var_dump($_PATCH);
        $bugManager = new BugManager();
        $bug = $bugManager->find($id);
        //if (isset($_PATCH)) {
            if (isset($_PATCH['description'])) {
                $bug->setDescription($_PATCH['description']);
            }
            
            if (isset($_PATCH['auteur'])) {
                $bug->setAuteur($_PATCH['auteur']);
            }

            if (isset($_PATCH['etat'])) {
                $bug->setEtat($_PATCH['etat']);
            }
        
            $bugManager->update($bug);

            http_response_code(200);
            header('Content-type: application/json');
            $response =[
                'succes' => true,
                'id' => $bug->getId(),
            ];
            echo json_encode($response);
        //}
        

        
    }

    public function render($templatePath, $params){
        $templatePath = $templatePath . ".php";
        $params;
        
        require($templatePath);

        return ob_get_clean();
    }

    public static function sendHttpResponse($content, $code = 200){
        http_response_code($code);

        header('Content-type: application/json');

        echo json_encode($content);
    }

    public function RequestApi($domainName){
        $client = new Client();
        $response = $client->request('GET', "http://ip-api.com/json/$domainName");

        //echo $response->getStatusCode(); // 200
        //echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
        //echo $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'

        $JsonResponse = $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'
        //print_r($responseAPI);
        return $responseAPI = json_decode($JsonResponse);
    }
   
    public function getDomainName($url){
       $resultParseUrl = parse_url($url);
       //var_dump($resultParseUrl);
       $domainName = $resultParseUrl['path'];

       //var_dump($domainName);

       return $domainName;
    }

}
