<?php
namespace BugApp\Models;
class Bug{
    public $id;
    public $description;
    public $auteur;
    public $etat = 0;
    public $createdAt;
    public $domainName;
    public $ip;
    
    /*function __construct($id, $description, $auteur, $etat,$createdAt) {
        $this->id = $id;
        $this->description = $description;
        $this->auteur = $auteur;
        $this->etat = $etat;
        $this->createdAt= $createdAt;       
    }*/

    function __construct(){

    }

        
    function getId() {
        return $this->id;
    }

    function getDescription() {
        return $this->description;
    }

    function getAuteur() {
        return $this->auteur;
    }

    function getEtat() {
        return $this->etat;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setAuteur($auteur) {
        $this->auteur = $auteur;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }       
     
    function getCreatedAt() {
        return $this->createdAt;
    }

    function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }


    public function getDomainName(){
        return $this->domainName;
    }

    public function setDomainName($domainName){
        $this->domainName = $domainName;
        return $this;
    }

    public function getIp(){
        return $this->ip;
    }

    public function setIp($ip){
        $this->ip = $ip;
        return $this;
    }
}

?>