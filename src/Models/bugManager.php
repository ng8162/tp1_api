<?php
namespace BugApp\Models;

use BugApp\Models\Bug;
use BugApp\Models\Manager;


class BugManager extends Manager{
    private $bugs=[];
    
    function __construct() {

        $this->connectDB();       
    }
   

    
    /**
     * Chatger les donnees du data.CSV
     * 
     */
    function loadCsv(){
        //recuperer le csv
        $fichier = 'C:\xampp\htdocs\dashboard\works\tp1\data.csv';

        $csv = new \SplFileObject($fichier);
        $csv->setFlags(\SplFileObject::READ_CSV);
        $csv->setCsvControl(';');
        //pour chaque ligne 
        foreach($csv as $ligne){
            
            if ($ligne[0]!=null) {
                //creer un un bug
               $bug=new Bug((int)$ligne[0], $ligne[1]);

                //inserer le bug dans this->$bugs
                //ajout du bug dans un tableau
                array_push($this->bugs, $bug);
            } 
            
        }
        return $this->bugs;

        
    }
    function findAll(){        

        // On récupère tout le contenu de la table jeux_video
        $reponse = $this->bdd->query('SELECT * FROM buglist');

        /*// On affiche chaque entrée une à une
        while ($donnees = $reponse->fetch())
        {
            //echo $donnees['id'].' '. $donnees['description'].'<br>';
            //creer un un bug
            $bug=new Bug((int)$donnees['id'], $donnees['description'], $donnees['auteur'],$donnees['etat'],$donnees['created_at']);
            //ajout du bug dans un tableau
            array_push($this->bugs, $bug);
        }

        $reponse->closeCursor();*/

    

     foreach ($reponse as $donnee) {
         $bug = new Bug();
         $bug->setId($donnee['id']);
         $bug->setDescription($donnee['description']);
         $bug->setAuteur($donnee['auteur']);
         $bug->setCreatedAt($donnee['created_at']);
         $bug->setEtat($donnee['etat']);
         $bug->setCreatedAt($donnee['created_at']);
         $bug->setDomainName($donnee['domainName']);
         $bug->setIp($donnee['ip']);

         
         //ajout du bug dans un tableau
         array_push($this->bugs, $bug);
     }     
        return $this->bugs;
          
    }
    function findByStatut($etat){        

        // On récupère tout le contenu de la table jeux_video
        $reponse = $this->bdd->query("SELECT * FROM `buglist` WHERE `etat` = $etat");
        /*// On affiche chaque entrée une à une
        while ($donnees = $reponse->fetch())
        {
            //echo $donnees['id'].' '. $donnees['description'].'<br>';
            //creer un un bug
            $bug=new Bug((int)$donnees['id'], $donnees['description'], $donnees['auteur'],$donnees['etat'],$donnees['created_at']);
            //ajout du bug dans un tableau
            array_push($this->bugs, $bug);
        }

        $reponse->closeCursor();*/

    

     foreach ($reponse as $donnee) {
         $bug = new Bug();
         $bug->setId($donnee['id']);
         $bug->setDescription($donnee['description']);
         $bug->setAuteur($donnee['auteur']);
         $bug->setCreatedAt($donnee['created_at']);
         $bug->setEtat($donnee['etat']);
         $bug->setDomainName($donnee['domainName']);
         $bug->setIp($donnee['ip']);
         
         //ajout du bug dans un tableau
         array_push($this->bugs, $bug);
     }     
        return $this->bugs;
          
    }

    function find($id){

        // On récupère tout le contenu de la table jeux_video
        $reponse = $this->bdd->query("SELECT * FROM buglist WHERE id = $id");
        
        $donnee = $reponse->fetch();
        /*// On affiche chaque entrée une à une
        while ($donnees = $reponse->fetch())
        {
            //echo $donnees['id'].' '. $donnees['description'].'<br>';
            //creer un un bug
            $bug=new Bug((int)$donnees['id'], $donnees['description'], $donnees['auteur'],$donnees['etat'],$donnees['created_at']);

        }

        $reponse->closeCursor();*/
        
         $bug = new Bug();
         $bug->setId($donnee['id']);
         $bug->setDescription($donnee['description']);
         $bug->setAuteur($donnee['auteur']);
         $bug->setCreatedAt($donnee['created_at']);
         $bug->setEtat($donnee['etat']);
         $bug->setDomainName($donnee['domainName']);
         $bug->setIp($donnee['ip']);
        
        return $bug; 
    }

    /**
     * Ajouter un bug
     * @param Bug $bug
     */
    function add($description, $auteur, $etat,$domaineName,$ip){
        
        $req = $this->bdd->prepare("INSERT INTO `buglist` (`description`, `auteur`, `etat`, domainName , ip ) VALUES (:description , :auteur, :etat, :domainName, :ip)");
        $req->execute(array(
	        'description' => $description,
	        'auteur' => $auteur,
            'etat' => $etat,
            'domainName' => $domaineName,
            'ip' => $ip
	    ));
    }

    function update($bug){

        $sql = 'UPDATE buglist SET description = :description, auteur = :auteur, etat = :etat, domainName = :domainName, ip = :ip  where id = :id';
        $sth = $this->bdd->prepare($sql);
        $sth->execute([
            'description'=> strval($bug->getDescription()),
            'auteur' => strval($bug->getAuteur()),
            'etat' => intval($bug->getEtat()),
            'id'=>intval($bug->getId()),
            'domainName'=> strval($bug->getDomainName()),
            'ip' => strval($bug->getIp())
        ]);

    }
}