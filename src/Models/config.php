<?php
namespace BugApp\Models;
class Config{
    
    private $host="localhost";
    private $dbname = "bug";
    private $login = 'root';
    private $password = '';
    
    
    function __construct() {
        
    }
    
    function getHost() {
        return $this->host;
    }

    function getDbname() {
        return $this->dbname;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassword() {
        return $this->password;
    }

    function setHost($host) {
        $this->host = $host;
    }

    function setDbname($dbname) {
        $this->dbname = $dbname;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setPassword($password) {
        $this->password = $password;
    }



}



?>