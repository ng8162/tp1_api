<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../src/Ressources/css/style.css">
    <title>Ajouter un Bug</title>
</head>
<body>

<h1><span class="blue">&lt;</span>Bugs<span class="blue">&gt;</span> <span class="yellow">Add</span></h1>
<h2>Created by <a href="#" target="_blank">Nicolas glories</a></h2>
<div class="login">

    <form method="POST" action="#">
        <label>Description</label>
        <textarea id="story" name="description" rows="5" cols="42" ></textarea>

        <label>Auteur</label>
        <input type="email" name="auteur" id="" value="">
        <div>
            <label for="horns">etat</label>
            <input type="checkbox" id="horns" name="etat">
        </div>

        <label>Nom de domaine</label>
        <input type="text" name="domainName" id="" value="">

        <button type="submit" class="btn btn-primary">valider</button>
        <button class="btn btn-primary"><a href="../bug/list">retour</a></button>

    </form> 
</div>
    
    
</body>
</html>